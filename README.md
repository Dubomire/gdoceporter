# GDocExporter

![doc.png](readme_images/doc.png) ![azure.png](readme_images/azure.png)

![net.png](readme_images/net.png)

GDocExporter is a highly targeted application that allows you to parse Google documents and create corresponding tasks in AureDevops.

### Installation

- You can clone repository and build application on your machine
- Or you can download builded application: https://drive.google.com/drive/folders/1TK5vt9y_jW4RRY2LKe41jX0vLFMV_iGr?usp=sharing

### Requirements

GDocExporter requires [AzureCLI](https://docs.microsoft.com/ru-ru/cli/azure/install-azure-cli-windows?tabs=azure-cli) to run.

To check that AzureCLI is installed run the command.

```sh
az --version
```

### Run

![exp.png](readme_images/exp.png)

1. Copy ID of your's Google document 
2. Run GDocExporter.exe 
3. Fill all boxes 
4. Press 'Export' button
5. GDocExporter will ask you to login in Google and give an access to the application
6. GDocExporter will ask you to login in Azure DevOps
7. Done

![id.png](readme_images/id.png)

### Google document structure

**ATTENTION!!!**

The application is at the prototype stage. Therefore, it is very important to strictly observe the structure of the document.

Template: https://docs.google.com/document/d/1LmARcepMYsRvPflysvo0OXX2cpso6gE3pHsfsVgs5TM/edit?usp=sharing

The general structure of the document
1. Title
2. Acceptance critera
3. Tasks

##### 1. Title

Title should contain PBI number included in square brackets

![pbi.png](readme_images/pbi.png)

##### 2. Acceptance critera

Acceptance critera should be under tag [AC] and each AC should be on a separate line.

##### 3. Tasks

Tasks should be described as table.


Each task should contain
* Task name (first row, first column) after [Tsk] tag
* Estimate number (first row, second column)
* Description (second row)

**EACH CELL SHOULD CONTAIN SOME VALUE, IT SHOULDN'T BE EMPTY!!!**

