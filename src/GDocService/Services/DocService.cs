﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Docs.v1;
using Google.Apis.Docs.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.IO;
using System.Threading;
using GDocService.Exceptions;
using Google.Apis.Requests;

namespace GDocService.Services
{
    /// <summary>
    /// Document service. Allows send request to GoogleDoc API to receive documents.
    /// </summary>
    public static class DocService
    {
        #region private members

        private static readonly string[] Scopes = { DocsService.Scope.DocumentsReadonly };
        private const string ApplicationName = "GDocExporter";

        private static readonly DocsService Service;

        static DocService()
        {
            UserCredential credential;

            using (var stream =
                new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = "token.json";

                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    Scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
            }

            Service = new DocsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = ApplicationName,
            });
        }

        #endregion

        #region public members

        /// <summary>
        /// Get specified document.
        /// </summary>
        public static Document GetDocument(string documentId)
        {
            DocumentsResource.GetRequest request = Service.Documents.Get(documentId);
            Document document = new Document();

            try
            {
                return request.Execute();
            }
            catch (Exception)
            {
                throw new DocumentNotFoundException(
                    "Cannot export document by specified identifier");
            }
        }

        #endregion
    }
}
