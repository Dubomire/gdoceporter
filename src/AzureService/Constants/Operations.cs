﻿namespace AzureService.Constants
{
    /// <summary>
    /// Operations for Azure API contained in request.
    /// </summary>
    public static class Operations
    {
        #region public members

        /// <summary>
        /// This operation allows add new or change exist field of work item.
        /// </summary>
        public const string Add = "add";

        #endregion
    }
}
