﻿namespace AzureService.Constants
{
    /// <summary>
    /// Paths for Azure API requests. Defines which field of work item will be changed.
    /// </summary>
    public static class Paths
    {
        #region public members

        public const string Title = "/fields/System.Title";
        public const string Tags = "/fields/System.Tags";
        public const string Area = "/fields/System.AreaPath";
        public const string Iteration = "/fields/System.IterationPath";
        public const string RemainingWork = "/fields/Microsoft.VSTS.Scheduling.RemainingWork";
        public const string Activity = "/fields/Microsoft.VSTS.Common.Activity";
        public const string TargetBranch = "/fields/FOSS.Common.TargetBranch";
        public const string Description = "/fields/System.Description";
        public const string Relations = "/relations/-";
        public const string AcceptanceCriteria = "/fields/Microsoft.VSTS.Common.AcceptanceCriteria";

        #endregion
    }
}
