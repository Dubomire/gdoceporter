﻿using System.Text.Json;
using AzureService.Models;

namespace AzureService.Helpers
{
    /// <summary>
    /// Contains common help methods.
    /// </summary>
    public static class CommonHelper
    {
        #region public members

        /// <summary>
        /// Runs specified command and returns console response.
        /// </summary>
        public static string RunCommand(string command)
        {
            System.Diagnostics.ProcessStartInfo procStartInfo =
                new System.Diagnostics.ProcessStartInfo("cmd", "/c " + command);
            procStartInfo.RedirectStandardOutput = true;
            procStartInfo.UseShellExecute = false;
            procStartInfo.CreateNoWindow = true;
            System.Diagnostics.Process proc = new System.Diagnostics.Process();
            proc.StartInfo = procStartInfo;
            proc.Start();

            return proc.StandardOutput.ReadToEnd();
        }

        /// <summary>
        /// Gets token from specified Azure CLI json.
        /// </summary>
        public static string GetToken(string json)
        {
            var serializeOptions = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var accessTokenResponse = JsonSerializer.Deserialize<AccessTokenResponse>(
                json,
                serializeOptions);
            return accessTokenResponse.AccessToken;
        }

        /// <summary>
        /// Gets WorkItem URL from specified Azure API json.
        /// </summary>
        public static string GetWorkItemUrl(string json)
        {
            var serializeOptions = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };
            var workItemResponse = JsonSerializer.Deserialize<WorkItemResponse>(
                json,
                serializeOptions);
            return workItemResponse.Url;
        }

        #endregion
    }
}
