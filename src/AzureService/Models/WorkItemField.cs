﻿using System.Text.Json.Serialization;

namespace AzureService.Models
{
    /// <summary>
    /// Work item field model. Request to Azure API consists of list of these fields.
    /// </summary>
    public class WorkItemField
    {
        #region public members

        /// <summary>
        /// Defines operation.
        /// </summary>
        [JsonPropertyName("op")]
        public string Operation { get; set; }

        /// <summary>
        /// Defines field to changed.
        /// </summary>
        [JsonPropertyName("path")]
        public string Path { get; set; }

        /// <summary>
        /// Defined value to be setted.
        /// </summary>
        [JsonPropertyName("value")]
        public object Value { get; set; }

        #endregion
    }
}
