﻿namespace AzureService.Models
{
    /// <summary>
    /// WorkItem response model received via Azure API..
    /// </summary>
    class WorkItemResponse
    {
        #region public members

        /// <summary>
        /// Url.
        /// </summary>
        public string Url { get; set; }

        #endregion
    }
}
