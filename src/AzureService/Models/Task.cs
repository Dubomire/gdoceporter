﻿using System.Linq;

namespace AzureService.Models
{
    /// <summary>
    /// Task model. Used for new tasks creation.
    /// </summary>
    public class Task
    {
        #region private members

        private const int ShortTitleLength = 60;

        #endregion

        #region public members

        /// <summary>
        /// Task title.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Short title.
        /// </summary>
        public string ShortTitle
        {
            get
            {
                if (Title.Length > ShortTitleLength)
                {
                    return Title.Substring(0, ShortTitleLength) + "...";
                }

                return Title;
            }
        }

        /// <summary>
        /// Task tags.
        /// </summary>
        public string Tags { get; set; }

        /// <summary>
        /// Task work area.
        /// </summary>
        public string Area { get; set; }

        /// <summary>
        /// Task Iteration.
        /// </summary>
        public string Iteration { get; set; }

        /// <summary>
        /// Task remaining work in hours.
        /// </summary>
        public double RemainingWork { get; set; }

        /// <summary>
        /// Task activity type.
        /// </summary>
        public string Activity { get; set; }

        /// <summary>
        /// Task target branch.
        /// </summary>
        public string TargetBranch { get; set; }

        /// <summary>
        /// Task description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Create new task or update existent one.
        /// </summary>
        public bool IsNeedToCreateNew { get; set; } = true;

        #endregion
    }
}
