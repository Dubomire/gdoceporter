﻿namespace AzureService.Models
{
    /// <summary>
    /// PBI model. Used for update PBI fields.
    /// </summary>
    public class Pbi
    {
        #region public members

        public int Id { get; set; }
        public string AcceptanceCriteria { get; set; }
        public int EndWeek { get; set; }
        public int Estimate { get; set; }

        #endregion
    }
}
