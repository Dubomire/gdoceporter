﻿using System.Text.Json;
using System.Net.Http;
using System.Text;
using AzureService.Models;
using System.Net.Http.Headers;
using System.Collections.Generic;
using System.Globalization;
using AzureService.Helpers;
using AzureService.Constants;

namespace AzureService.Services
{
    /// <summary>
    /// Service allows send requests to Azure API. Allows create or update WorkItems.
    /// </summary>
    public static class WorkItemService
    {
        #region private members
        private static readonly HttpClient Client;


        static WorkItemService()
        {
            Client = new HttpClient();
        }

        private static string GetPbiUrl(int pbiId)
        {
            var url = string.Format(Urls.WorkItemUrl, pbiId);
            var response = Client.GetAsync(url);
            response.Wait();

            var task = response.Result.Content.ReadAsStringAsync();
            task.Wait();

            return CommonHelper.GetWorkItemUrl(task.Result);
        }

        /// <summary>
        /// Creates specified Task linked to specified PBI as a child.
        /// </summary>
        private static void CreateWorkItem(Task task, string pbiUrl)
        {
            var workItemOptions = new List<WorkItemField>
            {
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.Title,
                    Value = task.Title
                },
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.Activity,
                    Value = task.Activity
                },
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.Area,
                    Value = task.Area
                },
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.Description,
                    Value = task.Description
                },
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.Iteration,
                    Value = task.Iteration
                },
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.RemainingWork,
                    Value = task.RemainingWork.ToString(CultureInfo.InvariantCulture)
                },
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.Tags,
                    Value = task.Tags
                },
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.TargetBranch,
                    Value = task.TargetBranch
                },
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.Relations,
                    Value = new Link
                    {
                        Relation = LinkTypes.Child,
                        Url = pbiUrl
                    },
                }
            };

            var json = JsonSerializer.Serialize(workItemOptions, new JsonSerializerOptions { WriteIndented = true });
            var content = new StringContent(json, Encoding.UTF8, "application/json-patch+json");

            var response = Client.PostAsync(Urls.NewWorkItemUrl, content);
            response.Wait();
        }
        #endregion

        #region public members

        /// <summary>
        /// Login in Azure and set token.
        /// </summary>
        public static void LoginInAzure()
        {
            CommonHelper.RunCommand(AzCommands.Login);
            var token = CommonHelper.GetToken(CommonHelper.RunCommand(AzCommands.GetToken));
            Client.DefaultRequestHeaders.Authorization =
                new AuthenticationHeaderValue("Bearer", token);
        }


        /// <summary>
        /// Create specified WorkItems linked to specified PBI as a child.
        /// </summary>
        public static void CreateWorkItems(List<Task> workItems, int pbiId)
        {
            string pbiUrl = GetPbiUrl(pbiId);

            foreach (var workItem in workItems)
            {
                CreateWorkItem(workItem, pbiUrl);
            }
        }

        /// <summary>
        /// Update PBI.
        /// </summary>
        public static void UpdateWorkItem(Pbi pbi)
        {
            var workItemOptions = new List<WorkItemField>
            {
                new WorkItemField
                {
                    Operation = Operations.Add,
                    Path = Paths.AcceptanceCriteria,
                    Value = pbi.AcceptanceCriteria
                }
            };

            var json = JsonSerializer.Serialize(workItemOptions, new JsonSerializerOptions { WriteIndented = true });
            var content = new StringContent(json, Encoding.UTF8, "application/json-patch+json");

            var url = string.Format(Urls.WorkItemUrl, pbi.Id);
            var response = Client.PatchAsync(url, content);
            response.Wait();
        }
        #endregion
    }
}
