﻿namespace ExportService.Constants
{
    /// <summary>
    /// Tags placed in Google document. Defines type of section in document.
    /// </summary>
    public static class DocTags
    {
        #region public members

        public const string AC = "[ac]";
        public const string Task = "[tsk]";

        #endregion
    }
}
