﻿namespace ExportService.Constants
{
    /// <summary>
    /// Patterns for regex search in Google document.
    /// </summary>
    public static class Patterns
    {
        #region public members

        public const string PBI = @"\[\s*PBI\s*\d{6}\s*]";
        public const string Line = @"\n*[^\n]+\n*";

        #endregion
    }
}
