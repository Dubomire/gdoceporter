﻿using System;
using System.Windows;
using GDocExporter.Forms;

namespace ExportService
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                MainWindow mainWindow = new MainWindow();
                mainWindow.ShowDialog();
            }
            catch (Exception exception)
            {
                MessageBox.Show(
                    exception.Message,
                    "Ooops...", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
        }
    }
}