﻿using System;

namespace ExportService.Exceptions
{
    /// <summary>
    /// Document format exception.
    /// </summary>
    public class DocumentFormatException : Exception
    {
        public DocumentFormatException(string message) : base(message) { }
    }
}
