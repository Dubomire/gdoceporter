﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using AzureService.Models;
using AzureService.Services;
using ExportService.Converters;
using GDocService.Services;

namespace ExportService.Services
{
    /// <summary>
    /// Main service. Allows export document from GoogleDoc to Azure DevOps.
    /// </summary>
    public static class Exporter
    {
        #region public members

        public static (Pbi pbi, List<Task> tasks) ExportTasks(string googleDocId, ProgressBar progressBar)
        {
            // TODO: rework progress bar logic
            progressBar.Value = 10;

            var document =
                DocService.GetDocument(googleDocId);
            progressBar.Value = 50;

            var result = DocConverter.Convert(document);
            progressBar.Value = 100;

            return result;
        }

        public static void ImportTasks(Pbi pbi, List<Task> tasks, ProgressBar progressBar)
        {
            // TODO: rework progress bar logic

            progressBar.Value = 10;
            WorkItemService.LoginInAzure();
            progressBar.Value = 33;
            WorkItemService.CreateWorkItems(tasks, pbi.Id);
            progressBar.Value = 66;
            WorkItemService.UpdateWorkItem(pbi);
            progressBar.Value = 100;
        }

        #endregion
    }
}
