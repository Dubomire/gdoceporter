﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using AzureService.Models;
using ExportService.Constants;
using ExportService.Exceptions;
using GDocExporter;
using Google.Apis.Docs.v1.Data;

namespace ExportService.Converters
{
    /// <summary>
    /// Document converter. Allows convert Google document to Tasks and PBI.
    /// </summary>
    public static class DocConverter
    {
        #region private members

        private static string GetTextFromParagraph(IList<StructuralElement> elements, bool isWithStyles = false)
        {
            return elements.Aggregate("", (current, element) =>
                current + GetTextFromParagraph(element?.Paragraph, isWithStyles));
        }

        private static string GetTextFromParagraph(Paragraph paragraph, bool isWithStyles = false)
        {
            if (paragraph == null)
            {
                return string.Empty;
            }

            return paragraph.Elements.Aggregate("", (current, element) =>
                current + element?.TextRun?.Content);
        }

        private static string StyleText(TextRun textRun)
        {
            if (textRun?.Content == null)
            {
                return null;
            }

            string text = textRun.Content;

            if (textRun.TextStyle.Bold ?? false)
            {
                text += "<b>" + text + "</b>";
            }

            if (textRun.TextStyle.Italic ?? false)
            {
                text += "<i>" + text + "</i>";
            }

            if (textRun.TextStyle.Underline ?? false)
            {
                text += "<u>" + text + "</u>";
            }

            return text;
        }

        private static Pbi CreatePbi(string text, int id)
        {
            string acceptanceCriteria =
                text.Substring(
                    text.IndexOf(
                        DocTags.AC,
                        StringComparison.OrdinalIgnoreCase)
                    + DocTags.AC.Length);

            return new Pbi()
            {
                Id = id,
                AcceptanceCriteria = acceptanceCriteria
                //EndWeek = ___Coming soon___
                //Estimate = ___Coming soon___
            };
        }

        private static void AddDefaultOptions(Task task)
        {
            task.Activity = Settings.Default.Activity;
            task.Area = Settings.Default.Area;
            task.Tags = Settings.Default.Tags;
            task.TargetBranch = Settings.Default.TargetBranch;
            task.Iteration = Settings.Default.Iteration;
        }

        private static int CalculateEndWeek(double summaryRemainingWork)
        {
            throw new NotImplementedException();
        }

        private static int GetPbiId(IList<StructuralElement> elements)
        {
            string text = GetTextFromParagraph(elements);

            Regex rx = new Regex(
                Patterns.PBI,
                RegexOptions.IgnoreCase);

            string pbiId = Regex.Match(rx.Match(text).Value, @"\d{6}").Value;

            return int.Parse(pbiId);
        }

        private static List<Task> GetTasks(IList<StructuralElement> documentElements)
        {
            Table taskTable = documentElements.First(e => e.Table != null).Table;
            var tasks = new List<Task>();

            foreach (var row in taskTable.TableRows)
            {
                string text = GetTextFromParagraph(row.TableCells[0]?.Content);

                if (text.Contains(DocTags.Task, StringComparison.OrdinalIgnoreCase))
                {
                    // Process task title.

                    // Remove task tag.
                    text = text.Replace(
                            DocTags.Task,
                            "",
                            StringComparison.OrdinalIgnoreCase)
                        .Replace("\n", "");

                    var currentWorkItem = new Task
                    {
                        Title = text
                    };

                    // Process task remaining work.

                    // Remove whitespaces.
                    string remainingWorkText = Regex.Replace(
                        input: GetTextFromParagraph(row.TableCells[1].Content[0].Paragraph),
                        pattern: @"\s+",
                        replacement: "");

                    // Replace "," by "."
                    remainingWorkText = Regex.Replace(
                        input: remainingWorkText,
                        pattern: @",",
                        replacement: ".");

                    if (string.IsNullOrEmpty(remainingWorkText))
                    {
                        remainingWorkText = "0.0";
                    }

                    if (!double.TryParse(
                        remainingWorkText,
                        NumberStyles.Any,
                        CultureInfo.InvariantCulture,
                        out double remainingWork))
                    {
                        throw new DocumentFormatException(
                            $"Wrong RemainingWork cell format.\nIncorrect value: {remainingWorkText}");
                    }

                    currentWorkItem.RemainingWork = remainingWork;

                    AddDefaultOptions(currentWorkItem);
                    tasks.Add(currentWorkItem);
                }
                else
                {
                    // Convert description text to HTML format.
                    text = ConvertToHtml(row.TableCells[0].Content);

                    tasks.Last().Description = text;
                }
            }

            return tasks;
        }

        private static string ConvertToHtml(IList<StructuralElement> elements)
        {
            string text = string.Empty;

            for (int i = 0; i < elements.Count; i++)
            {
                Paragraph paragraph = elements[i]?.Paragraph;
                if (paragraph == null)
                {
                    continue;
                }

                if (paragraph.Bullet?.ListId == null)
                {
                    text += GetTextFromParagraph(paragraph, true)
                        .Replace("\n", "<br>");
                    continue;
                }

                text += GetList(elements, i, out i);
            }

            return text;
        }

        private static string GetList(
            IList<StructuralElement> elements,
            int startListPosition,
            out int endListPosition)
        {
            int currentLevel = 0;
            int i = startListPosition;

            string text = "<ol>";

            while (i < elements.Count &&  elements[i].Paragraph?.Bullet?.ListId != null)
            {
                Paragraph paragraph = elements[i].Paragraph;
                int paragraphLevel = paragraph.Bullet?.NestingLevel ?? 0;

                if (paragraphLevel > currentLevel)
                {
                    text += "<ol>";
                }

                if (paragraphLevel < currentLevel)
                {
                    for (var j = 0; j < currentLevel - paragraphLevel; j++)
                    {
                        text += "</ol>";
                    }
                }

                text += "<li>";
                text += GetTextFromParagraph(paragraph)
                    .Replace("\n", "")
                    .Replace("\v", "<br>");
                text += "</li>";

                currentLevel = paragraphLevel;
                i++;
            }

            for (var j = 0; j < currentLevel + 1; j++)
            {
                text += "</ol>";
            }

            endListPosition = i-1;
            return text;
        }

        #endregion

        #region public members

        /// <summary>
        /// Convert document to tasks and parent PBI.
        /// </summary>
        public static (Pbi Pbi, List<Task> Tasks) Convert(Document document)
        {
            Pbi pbi =
                CreatePbi(
                    ConvertToHtml(document.Body.Content),
                    GetPbiId(document.Body.Content));

            List<Task> tasks = GetTasks(document.Body.Content);

            return (pbi, tasks);
        }

        #endregion
    }
}
