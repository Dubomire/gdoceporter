﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using AzureService.Models;
using ExportService.Services;
using GDocService.Exceptions;

namespace GDocExporter.Forms
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            InitializeComponent();

            InitializeSetting(Settings.Default.GDocIdentifier, GDocIdentifierTextBox);
            InitializeSetting(Settings.Default.Activity, ActivityTextBox);
            InitializeSetting(Settings.Default.Area, AreaTextBox);
            InitializeSetting(Settings.Default.Iteration, IterationTextBox);
            InitializeSetting(Settings.Default.TargetBranch, TargetBranchTextBox);
            InitializeSetting(Settings.Default.Tags, TagsTextBox);
        }

        private Pbi Pbi { get; set; }

        private List<Task> _tasks;
        private List<Task> Tasks
        {
            get => _tasks;
            set
            {
                _tasks = value;
                TaskListBox.ItemsSource = value;
            }
        }

        private bool _isDocExported = false;
        private bool IsDocExported
        {
            set
            {
                _isDocExported = value;
                ExportImportButton.Content = value ? "Import" : "Export";
            }
        }

        private void InitializeSetting(string value, TextBox textBox)
        {
            if (!string.IsNullOrEmpty(value))
            {
                textBox.Text = value;
            }
        }

        private  void SaveSettings()
        {
            Settings.Default.GDocIdentifier = GDocIdentifierTextBox.Text;
            Settings.Default.Activity = ActivityTextBox.Text;
            Settings.Default.Area = AreaTextBox.Text;
            Settings.Default.Iteration = IterationTextBox.Text;
            Settings.Default.Tags = TagsTextBox.Text;
            Settings.Default.TargetBranch = TargetBranchTextBox.Text;

            Settings.Default.Save();
        }

        private void Export()
        {
            IsEnabled = false;
            SaveSettings();

            ProgressBarWindow progressBarWindow = new ProgressBarWindow();
            progressBarWindow.Title = "Import progress";
            progressBarWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            progressBarWindow.Show();

            try
            {
                (Pbi, Tasks) = Exporter.ExportTasks(GDocIdentifierTextBox.Text, progressBarWindow.GetProgressBar());
                IsDocExported = true;

                MessageBox.Show(
                    "Tasks exported successful",
                    "Successful", MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }
            catch (DocumentNotFoundException exception)
            {
                MessageBox.Show(
                    exception.Message,
                    "Ooops...", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
            finally
            {
                progressBarWindow.Close();
                IsEnabled = true;
            }
        }

        private void Import()
        {
            IsEnabled = false;
            SaveSettings();

            ProgressBarWindow progressBarWindow = new ProgressBarWindow();
            progressBarWindow.Title = "Import progress";
            progressBarWindow.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            progressBarWindow.Show();

            try
            {
                Exporter.ImportTasks(Pbi, Tasks, progressBarWindow.GetProgressBar());

                MessageBox.Show(
                    "Tasks imported successful",
                    "Successful", MessageBoxButton.OK,
                    MessageBoxImage.Information);
            }
            catch (Exception exception)
            {
                // TODO: add custom exception.
                MessageBox.Show(
                    exception.Message,
                    "Ooops...", MessageBoxButton.OK,
                    MessageBoxImage.Error);
            }
            finally
            {
                progressBarWindow.Close();
                IsEnabled = true;
            }
        }

        private  void ExportImportButton_Click(object sender, RoutedEventArgs e)
        {
            if (_isDocExported)
            {
                Import();
            }
            else
            {
                Export();
            }
        }

        private void ResetButton_Click(object sender, RoutedEventArgs e)
        {
            IsDocExported = false;
            Tasks = new List<Task>();
        }

        private void CheckBox_Loaded(object sender, RoutedEventArgs e)
        {
            CheckBox checkBox = (sender as CheckBox);

            if (checkBox?.IsChecked != null && checkBox.IsChecked.Value)
            {
                checkBox.IsEnabled = false;
            }
        }
    }
}
